# mobile_challenge

Desafio de desenvolvimento Mobile para Coodesh 

## Getting Started

O desafio consiste na elaboração de uma aplicação mobile para visualização dos pacientes da empresa Pharma INC. 
O projeto será composto por três componentes principais: 
    - Uma tela de splash para exibição da logo da empresa; 
    - Uma lista dinâmica para exibição dos pacientes e um buscador. 
    - Um card para exibição dos dados do paciente. 


## Tecnologia 

Para elaboração do projeto foi utilizado Flutter. 

## Guia de utilização 

Nota: recomendo realizar todas as operações abaixo com permissões de administrador. 

Para que seja possível executar o projeto existem alguns pré-requisitos.  
    - Instalação do Android Studio -> https://developer.android.com/studio
    - Instalação dos plugins Flutter e Dart no Android Studio. 
    - Baixar a SDK do Flutter -> https://flutter.dev/docs/get-started/install
        - Descompacte a pasta e execute "flutter_console.bat" (dentro da pasta Flutter)
            - Execute o comando "Flutter doctor -v" e observe se precisa realizar alguma configuração adicional (geralmente só é necessário aceitar as licenças do android -> `flutter doctor --android-licenses`)



Em seguida abra o projeto clicando em "Open an Existing Project"

Com o projeto já aberto no Android Studio configure a SDK do Android. 
    - File -> Settings -> Appearance & Behavior -> System Settings -> Android SDK (verifique tanto SKD Platforms e SDK Tools)
    - Ainda em configurações acesse Languages & Frameworks e defina: 
        - Dart -> Dart SKD path: C:\Users\SEU USUARIO\Desktop\flutter\bin\cache\dart-sdk
        - Flutter -> Flutter SDK path: C:\Users\SEU USUARIO\Desktop\flutter

Pronto! Se tudo deu certo seu projeto já deve estar perto de poder compilar. 
Todavia, vamos garantir: com o terminal do flutter aberto ("flutter_console.bat") acesse a pasta do projeto e execute os seguintes comandos: 
    - flutter clean
    - flutter upgrade
    - flutter pub get (importante)

    Agora basta abrir um emulador e executar o comando abaixo: 
    - flutter run

    obs: caso pretenda executar o projeto no seu celular lembre-se se ativar a depuração por usb e conectar o celular ao computador. 
