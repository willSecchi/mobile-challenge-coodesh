import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserScreen extends StatefulWidget {
  final String imagem;
  final String nome;
  final String email;
  final String genero;
  final String dataNasc;
  final String telefone;
  final String nacionalidade;
  final String endereco;
  final String id;
  final String idade;

  UserScreen(this.imagem, this.nome, this.email, this.genero, this.dataNasc,
      this.telefone, this.nacionalidade, this.endereco, this.id, this.idade);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          widget.imagem,
          widget.nome,
          widget.email,
          widget.genero,
          widget.dataNasc,
          widget.telefone,
          widget.nacionalidade,
          widget.endereco,
          widget.id,
          widget.idade),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "E-mail",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              "${widget.email}",
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              "Data de Nascimento",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text("${widget.dataNasc}"),
            SizedBox(
              height: 16,
            ),
            Text(
              "Nacionalidade",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text("${widget.nacionalidade}"),
            SizedBox(
              height: 16,
            ),
            Text(
              "Endereço",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text("${widget.endereco}, ${widget.nacionalidade}"),
            SizedBox(
              height: 16,
            ),
            Text(
              "Telefone",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text("${widget.telefone}"),
            SizedBox(
              height: 16,
            ),
            Text(
              "ID",
              style: _style(),
            ),
            SizedBox(
              height: 4,
            ),
            Text("${widget.id}"),
            SizedBox(
              height: 16,
            ),
            Divider(
              color: Colors.grey,
            )
          ],
        ),
      ),
    );
  }

  _style() {
    return TextStyle(fontWeight: FontWeight.bold);
  }
}

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String imagem;
  final String nome;
  final String email;
  final String genero;
  final String dataNasc;
  final String telefone;
  final String nacionalidade;
  final String endereco;
  final String id;
  final String idade;

  CustomAppBar(this.imagem, this.nome, this.email, this.genero, this.dataNasc,
      this.telefone, this.nacionalidade, this.endereco, this.id, this.idade);

  @override
  Size get preferredSize => Size(double.infinity, 320);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Informações Cadastrais"),
        backgroundColor: Color(0xFF003461),
        centerTitle: true,
      ),
      body: _bodyCustomAppBar(),
    );
  }

  _bodyCustomAppBar() {
    return ClipPath(
      clipper: MyClipper(),
      child: Container(
        padding: EdgeInsets.only(top: 15),
        decoration: BoxDecoration(color: Color(0xFF003461), boxShadow: [
          BoxShadow(color: Colors.red, blurRadius: 20, offset: Offset(0, 0))
        ]),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover, image: NetworkImage(imagem))),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "$nome",
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SizedBox(
                      child: Column(
                        children: [
                          Text(
                            "Gênero",
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "$genero",
                            style: TextStyle(fontSize: 24, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      child: Column(
                        children: [
                          Text(
                            "Idade",
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            "$idade",
                            style: TextStyle(fontSize: 24, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = Path();

    p.lineTo(3, size.height - 25);
    p.lineTo(size.width, size.height);

    p.lineTo(size.width, 0);

    p.close();

    return p;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
