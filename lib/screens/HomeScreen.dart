import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobile_challenge/screens/UserScreen.dart';
import 'package:mobile_challenge/utils/nav.dart';
import 'dart:convert';

import 'package:rflutter_alert/rflutter_alert.dart';

class UserList extends StatefulWidget {
  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  int _currentIndex = 0;
  bool atualiza = false;
  final String apiUrl = "https://randomuser.me/api/?results=50";

  Future<List<dynamic>> fetchUsers() async {
    var result = await http.get(apiUrl);
    return json.decode(result.body)['results'];
  }

  String _gender(dynamic user) {
    return user['gender'];
  }

  String _name(dynamic user) {
    return user['name']['title'] +
        " " +
        user['name']['first'] +
        " " +
        user['name']['last'];
  }

  String _location(dynamic user) {
    return user['location']['country'];
  }

  String _age(Map<dynamic, dynamic> user) {
    return user['dob']['age'].toString();
  }

  String _email(dynamic user) {
    return user['email'];
  }

  String _registered(Map<dynamic, dynamic> user) {
    return user['dob']['date'].toString();
  }

  String _phone(dynamic user) {
    return user['phone'].toString();
  }

  String _endereco(Map<dynamic, dynamic> user) {
    return "City " + user['location']['city'].toString();
  }

  String _id(dynamic user) {
    return user['id']['value'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottinNavigation(),
      appBar: _appBar(),
      body: _body(),
    );
  }

  _appBar() {
    return AppBar(
      automaticallyImplyLeading: false,
      centerTitle: true,
      title: Text('Pharma INC - Pacientes'),
      backgroundColor: Color(0xFF003461),
    );
  }

  _body() {
    return Container(
      child: FutureBuilder<List<dynamic>>(
        future: fetchUsers(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return RefreshIndicator(
              onRefresh: _onRefresh,
              child: ListView.builder(
                  padding: EdgeInsets.all(8),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    var generoPtBR = _gender(snapshot.data[index]);
                    if (generoPtBR == 'male') {
                      generoPtBR = 'Masculino';
                    } else {
                      generoPtBR = 'Feminino';
                    }
                    return Card(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            leading: CircleAvatar(
                                radius: 30,
                                backgroundImage: NetworkImage(
                                    snapshot.data[index]['picture']['large'])),
                            title: Text(_name(snapshot.data[index])),
                            //subtitle: Text(_gender(snapshot.data[index])),
                            subtitle: Text(generoPtBR),
                            trailing:
                                Text("Idade: " + _age(snapshot.data[index])),
                            onTap: () {
                              String imagem =
                                  '${snapshot.data[index]['picture']['large']}';
                              String nome = '${_name(snapshot.data[index])}';
                              String email = '${_email(snapshot.data[index])}';
                              String genero = '$generoPtBR';
                              String dataNasc =
                                  '${_registered(snapshot.data[index])}';
                              String telefone =
                                  '${_phone(snapshot.data[index])}';
                              String nacionalidade =
                                  '${_location(snapshot.data[index])}';
                              String idade = '${_age(snapshot.data[index])}';
                              String endereco =
                                  '${_endereco(snapshot.data[index])}';
                              String id = '${_id(snapshot.data[index])}';
                              if (id == 'null') {
                                id = 'ID não registrado!';
                              }

                              Alert(
                                context: context,
                                type: AlertType.info,
                                title: "ATENÇÃO",
                                desc:
                                    "Deseja acessar as informações do paciente ${_name(snapshot.data[index])}?",
                                buttons: [
                                  DialogButton(
                                    child: Text(
                                      "Sim",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13),
                                    ),
                                    onPressed: () => push(
                                            context,
                                            UserScreen(
                                                imagem,
                                                nome,
                                                email,
                                                genero,
                                                dataNasc,
                                                telefone,
                                                nacionalidade,
                                                endereco,
                                                id,
                                                idade))
                                        .then(
                                            (value) => Navigator.pop(context)),
                                    color: Color(0xFF0083CA),
                                  ),
                                  DialogButton(
                                    child: Text(
                                      "Fechar",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13),
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                    color: Color(0xFFFCAF17),
                                  )
                                ],
                              ).show();
                            },
                          )
                        ],
                      ),
                    );
                  }),
            );
          }
        },
      ),
    );
  }

  _bottinNavigation() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
      backgroundColor: Color(0xFF003461),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.white.withOpacity(.6),
      selectedFontSize: 15,
      unselectedFontSize: 15,
      onTap: (value) {
        // Respond to item press.
        setState(() => _currentIndex = value);
      },
      items: [
        BottomNavigationBarItem(
          // ignore: deprecated_member_use
          title: Text('Início'),
          icon: Icon(Icons.home),
        ),
      ],
    );
  }

  Future<void> _onRefresh() {
    return Future.delayed(Duration(seconds: 2), () {
      setState(() => atualiza = true);
    });
  }
}
